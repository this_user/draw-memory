// Copyright 2016 under the GPLv3 or greater, depending on which provides greater freedom for dicks.

#include    <stdio.h>
#include    <stdlib.h>
#include    <unistd.h>
#include    <time.h>
#include    <string.h>
#include    <math.h>

#define GiB    1073741824
#define MiB       1048576
#define points         29
// 1000000 =   1 sec
// 500000  = 1/2 sec
// 250000  = 1/4 sec
//#define time_step 250000

int main (int argc, char ** argv) {
	long int time_step = 250000;
	long int loop_times     = 1;
	float max_mem_usage;
	if (argc <= 1) {
		printf("Usage: draw-memory <GiB> <times> <time-step>\nIt accepts floating point values such as 5.2 or 0.5\n");
		printf("Time step is in milliseconds. The default is 250\n");
		return 1;
	}
	else {
		max_mem_usage = atof(argv[1]);
		if (max_mem_usage <= 0 ) {
			printf("Negative memory values aren't allowed!!!");
			return 1;
		}
	}

	if (argc == 3) {
		loop_times = atoi(argv[2]);
	}
	else if (argc == 4) {
		time_step   = 1000 * atoi(argv[3]);
		loop_times  = atoi(argv[2]);
	}



	// Compute the height of the balls
	float ball_radius = 0.36;
	int ball_steps = 6;
	float ball_array[ball_steps];

	for (int i = 0; i < ball_steps; i++) {
	// x starts at the value of ball_radius and goes down to 0
	float x = ball_radius - ball_radius/ball_steps * i;

	// Compute the height, using a similar formula to computing the Saggita(height of an arc)
	ball_array[i] = sqrt(ball_radius * ball_radius - x*x);
	//printf("ball_array[%i] = %f  x  = %f\n", i, ball_array[i], x);
	}
	float base_array[points] = // in GiB
		{ball_array[0],ball_array[1],ball_array[2],ball_array[3],ball_array[4],ball_array[5],ball_array[4],ball_array[3],ball_array[2],ball_array[1],ball_array[0],
		0.964, 1.0 ,1.0 ,0.939 ,1.0 ,1.0, 0.964,
		ball_array[0],ball_array[1],ball_array[2],ball_array[3],ball_array[4],ball_array[5],ball_array[4],ball_array[3],ball_array[2],ball_array[1],ball_array[0]};

	double malloc_step = 0.1; // in GiB, amount to allocate at a time
	long int malloc_B = malloc_step * GiB; // in Bytes
	float array[points];

	// Calculate the array based on the
	for (int i = 0; i < points; i++) {
		array[i] = max_mem_usage * (base_array[i] / malloc_step);
		//printf("array[%i]=%f in %f GiB\n", i, array[i], malloc_step);
	}

	int value = (int)points/malloc_step;
	unsigned int * myptr[value];
	unsigned long time = 0;

	// total_malloc is in malloc_step units
	unsigned long total_malloc = 0;
	int long long delta;
	// Loop it based on loop_count
	for (int loop_count = 0; loop_count < loop_times; loop_count++) {
		for (int i = 0; i < points; i++) {

			delta = array[i] - total_malloc;

			if (total_malloc < array[i]) {
				while ( delta != 0 ) {
					myptr[total_malloc] = calloc(1, malloc_B);
					memset(myptr[total_malloc], 'D', malloc_B);
					total_malloc++;
					//printf("Total malloc: %lu\n", total_malloc);
					delta = array[i] - total_malloc;
					printf("total_malloc > array[i]\tdelta: %lli\n", delta);
				}
	    	}
			else if (total_malloc > array[i]) {
				while ( delta != 0 ) {
					free(myptr[total_malloc - 1]);
					total_malloc = total_malloc - 1;
					delta = array[i] - total_malloc;
					printf("total_malloc > array[i]\tdelta: %lli\n", delta);
				}
			}

			usleep(time_step);
			time = time + time_step;
			printf("time: %lu\n", time/time_step);
		}
		sleep(1);
	}
}
