all:
	make linux
	make windows
windows:
	i686-w64-mingw32-gcc -O3 -lm -Wall draw-memory.c -o draw-memory-w32.exe
	x86_64-w64-mingw32-gcc -O3 -lm -Wall draw-memory.c -o draw-memory-w64.exe
linux:
	gcc -O3 -lm -Wall draw-memory.c -o draw-memory
