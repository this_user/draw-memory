## draw-memory
This program will draw a **phallus** on your memory graph.

The following assumes you are compiling this program using linux:
To compile for Linux run:   make linux
To compile for Windows run: make windows

To compile for both, run:   make all



```
#!

   Usage: draw-memory <GiB> <times> <time-step>
       It accepts floating point values such as 5.2 or 0.5
       Time step is in milliseconds. The default is 250
```